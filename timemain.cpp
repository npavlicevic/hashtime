#include "timemain.h"

Timemain::Timemain(){}

Timemain::~Timemain(){}

void Timemain::timeblake(Timeblake*timeblake,int argc,char**argv){
	timeblake->time(argv[2],argv[3],atoi(argv[4]),atoi(argv[5]));

	delete timeblake;
}

void Timemain::timekeccak(Timekeccak*timekeccak,int argc,char**argv){
	timekeccak->time(argv[2],argv[3],atoi(argv[4]),atoi(argv[5]));

	delete timekeccak;
}

void Timemain::main(int argc,char**argv){
	if(!strcmp(argv[1],"-b")){
		this->timeblake(new Timeblake(new Blake()),argc,argv);
	}else if(!strcmp(argv[1],"-k")){
		this->timekeccak(new Timekeccak(new Keccak()),argc,argv);
	}
	
}

