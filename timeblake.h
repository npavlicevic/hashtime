#include <iomanip>
#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include "../blake/blake.h"

using namespace std;

class Timeblake{
protected:
	Blake*blake;

public:
	Timeblake(Blake*blake);
	~Timeblake();

	void time(char*filename,char*resultfile,int tests,int samples);
};
