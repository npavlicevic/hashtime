#include "timeblake.h"

Timeblake::Timeblake(Blake*blake){
	this->blake=blake;
}

Timeblake::~Timeblake(){
	delete this->blake;
}

void Timeblake::time(char*filename,char*resultfile,int tests,int samples){
	unsigned int block_size_bytes=this->blake->get_block_size_bytes();
	unsigned int bytes_read;
	unsigned char*buffer=new unsigned char[block_size_bytes];
	long int start_time;
	long int time_difference;
	struct timespec gettime_now;
	unsigned int time=0;
	unsigned int counttests=0;
	unsigned int countsamples=0;

	ifstream file(filename,ifstream::binary);
	ofstream result(resultfile);

	for(counttests=0;counttests<tests;counttests++){
		time=0;
		for(countsamples=0;countsamples<samples;countsamples++){
			file.seekg(0,ios::beg);

			clock_gettime(CLOCK_REALTIME,&gettime_now);
			start_time=gettime_now.tv_nsec;
			
			while((bytes_read=
			file.read((char*)buffer,block_size_bytes).gcount())==
			block_size_bytes){
				this->blake
				->update_hash_block(buffer,bytes_read);
			}

			this->blake->final_hash_block(buffer,bytes_read);

			clock_gettime(CLOCK_REALTIME,&gettime_now);
			time_difference=gettime_now.tv_nsec-start_time;

			time+=time_difference;
		}

		time/=countsamples;
		
		if(counttests){
			result<<","<<time;
		}else{
			result<<time;
		}
	}

	result<<endl;

	delete buffer;

	file.close();

	result.close();
}
