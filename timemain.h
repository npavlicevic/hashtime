#include "timeblake.h"
#include "timekeccak.h"

class Timemain{
protected:
	void timeblake(Timeblake*timeblake,int argc,char**argv);
	void timekeccak(Timekeccak*timekeccak,int argc,char**argv);
public:
	Timemain();
	~Timemain();
	void main(int argc,char**argv);
};
