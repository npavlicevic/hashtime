#include <iomanip>
#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include "../keccak-hash/keccak.h"

using namespace std;

class Timekeccak{
protected:
	Keccak*keccak;
public:
	Timekeccak(Keccak*keccak);
	~Timekeccak();

	void time(char*filename,char*resultfile,int tests,int samples);
};
